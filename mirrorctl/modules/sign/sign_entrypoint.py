import argparse
import subprocess

def sign(args :argparse.Namespace) -> None:
	"""
	Workflow copied from https://gitlab.archlinux.org/archlinux/archiso/-/blob/e8801729840db7dcb4b31ea3cb7f8f3d7aaec587/archiso/mkarchiso#L1235-1259
	"""
	if not args.privkey.exists():
		raise ValueError(f"Could not access --privkey: {args.privkey}")

	subprocess.call([
		"openssl", "cms",
		"-sign",
		"-binary",
		"-nocerts",
		"-noattr",
		"-outform", "DER",
		"-out", "test.cms.sig",
		"-in", "test.txt",
		"-signer", str(args.certificate),
		"-inkey", str(args.privkey)
	])