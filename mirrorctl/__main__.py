import importlib
import sys
import pathlib

"""
This logic is here to allow for relative imports via the .git repo,
as well as installed versions as secondary lookup.
"""
if pathlib.Path('./mirrorctl/__init__.py').absolute().exists():
	spec = importlib.util.spec_from_file_location("mirrorctl", "./mirrorctl/__init__.py")
	mirrorctl = importlib.util.module_from_spec(spec)
	sys.modules["mirrorctl"] = mirrorctl
	spec.loader.exec_module(sys.modules["mirrorctl"])
else:
	import mirrorctl

if __name__ == '__main__':
	mirrorctl.run_as_a_module()